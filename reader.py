import cv2
import numpy as np
import pathlib

def render(im):
    cv2.imshow("output", im)
    cv2.waitKey(0)


def greyscale(rgb_im):
    g = cv2.cvtColor(rgb_im, cv2.COLOR_RGB2GRAY)
    thresh, t = cv2.threshold(g, 127, 255, cv2.THRESH_BINARY)
    return t


def find_staves(im):
    ys, xs = np.where(im == 0)
    return [{'position': (ys.min(), xs.min()), 'current_note': -1}]  # -1 corresponds to c in octave 2


def find_horizontal_lines(im, stave):
    y, x = stave['position']
    [ys] = np.where(im[y:, x] == 0)

    diff = ys[1:] - ys[:-1]
    redundant = np.where(diff == 1)[0]  # sometimes the horiz lines get two hits per line

    ys = list(ys + y)
    ysref = ys[:]

    for idx in redundant:
        ys.remove(ysref[idx])

    stave['horizontal_lines'] = ys

    diffs = int((np.array(ys[1:]) - np.array(ys[:-1])).mean())
    stave['spacing'] = diffs


def find_bar_lines(im, stave):
    y, xmin = stave['position']
    ys = stave['horizontal_lines']
    stave_im = im[ys[0]:ys[-1], xmin:]
    bars = []
    for x in range(1, stave_im.shape[1] - 1):
        if np.all(stave_im[:, x - 1:x + 2] == 0):  # bar lines are three pixels wide
            bars.append(x + xmin)
    stave['bar_lines'] = bars


def find_notes(im, stave):
    just_after_clef_info = stave['time'][1][1]  # first bar is a special case
    bar_lines = [(just_after_clef_info, stave['bar_lines'][0])]
    bar_lines.extend([(stave['bar_lines'][bar], stave['bar_lines'][bar + 1]) for bar in range(len(stave['bar_lines']) - 1)])

    notes = [find_note(im, stave, bar, stave['horizontal_lines']) for bar in bar_lines]
    return " ".join(notes)


def find_note(im, stave, ylimits, xlimits):

    for x in xlimits:  # remove horizontal lines
        im[x - 2:x + 2, :] = 255

    ymin, ymax = xlimits[0] - 3 * stave['spacing'], xlimits[-1] + 3 * stave['spacing']
    xmin, xmax = ylimits[0] + 2, ylimits[1] - 2

    upper, lower = match_template(im, load_image('semibreve.png'), xmin, ymin, xmax, ymax)

    xloc = (upper[0]+lower[0])/2

    current_line = xlimits[2]  # start search relative to middle b

    i = 0
    finished = False
    while not finished:
        distance = xloc - (current_line + (stave['spacing'] * i / 2))
        if abs(distance) > stave['spacing'] / 4:
            if distance < 0:
                i -= 1
            else:
                i += 1
        else:
            finished = True

    notes = ['b1', 'a1', 'g1', 'f1', 'e1', 'd1', 'c1']
    note = notes[i % len(notes)]

    index = stave['current_note']

    interval = get_interval(index, i)

    if interval < 0:
        char = r"'"
    else:
        char = r","

    if abs(interval)>=5:
        note = note[0]+"".join([char for _ in range(1+(abs(interval) - 5) // 8)])+note[1]

    stave['current_note'] = i
    return note


def match_template(im, template, xmin, ymin, xmax, ymax):
    # https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_imgproc/py_template_matching/py_template_matching.html?highlight=template%20matching
    cropped=im[ymin:ymax, xmin:xmax]
    w, h = template.shape
    res = cv2.matchTemplate(cropped, template, cv2.TM_SQDIFF)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

    top_left = min_loc[1]+ymin, min_loc[0]+xmin
    bottom_right = (top_left[0] + w, top_left[1] + h)

    return top_left, bottom_right


def find_treble_clef(im, stave_offset, first_bar_line, lowest_horizontal_line, spacing):
    template = greyscale(cv2.imread("treble_clef.png"))
    w, h = template.shape

    ymin, xmin = stave_offset
    xmax = first_bar_line
    ymax = lowest_horizontal_line + 2*spacing

    return match_template(im, template, xmin, ymin, xmax, ymax)


def find_time_signature(im, clef_position, first_bar_line):
    template = greyscale(cv2.imread("c_time.png"))

    (ymin, _), (ymax, xmin) = clef_position
    xmax = first_bar_line

    return match_template(im, template, xmin, ymin, xmax, ymax)


def write(note_string):
    path = pathlib.Path("output.ly")
    lines=[r"\relative c ''{"]
    lines.append(note_string)
    lines.append("}")
    path.write_text("\n".join(lines))


def get_staves_from_image(im):
    staves = find_staves(im)
    find_horizontal_lines(im, staves[0])
    find_bar_lines(im, staves[0])
    staves[0]['clef'] = find_treble_clef(im, staves[0]['position'],
                                         staves[0]['bar_lines'][0],
                                         staves[0]['horizontal_lines'][-1],
                                         staves[0]['spacing'])
    
    staves[0]['time'] = find_time_signature(im, staves[0]['clef'],
                                            staves[0]['bar_lines'][0])
    return staves


def load_image(name):
    return greyscale(cv2.imread(name))


def get_interval(a_index, b_index):
    if b_index > a_index:
        return (b_index - a_index) + 1
    else:
        return (b_index - a_index) - 1


