import pytest
from reader import *


@pytest.fixture
def rgb_im():
    """
    from wand.image import Image
    from wand.color import Color

    with Image(filename="test.pdf", resolution=300) as img:
        with Image(width=img.width, height=img.height, background=Color("white")) as bg:
            bg.composite(img, 0, 0)
            bg.save(filename="testwand.png")
    """
    return cv2.imread('test.png')


@pytest.fixture
def im(rgb_im):
    return greyscale(rgb_im)


@pytest.fixture
def descending_notes_im():
    return load_image("descending.png")


@pytest.fixture
def octave_jump_im():
    return load_image("octave_jump.png")


@pytest.fixture
def minim_im():
    return load_image("minim.png")


@pytest.fixture
def staves():
    def fun(im_under_test):
        return get_staves_from_image(im_under_test)

    return fun


def test_can_load_png_as_greyscale(rgb_im):
    assert len(greyscale(rgb_im).shape) == 2


def test_can_find_stave_lines(im):
    staves = find_staves(im)
    assert len(staves) == 1


def test_current_note_is_middle_c(im):
    assert find_staves(im)[0]['current_note'] == -1


def test_stave_line_contains_all_of_first_line(im, staves):
    staves_found = staves(im)
    assert staves_found[0]['position'] == (53, 197)


def test_stave_line_contains_all_of_first_line_when_im_is_offset(im):
    staves = find_staves(im[25:, :])
    assert staves[0]['position'] == (28, 197)


def test_can_find_5_horizontal_bar_lines_in_stave(im, staves):
    assert len(staves(im)[0]['horizontal_lines']) == 5


def test_can_find_horizontal_bar_lines_in_stave(im, staves):
    assert staves(im)[0]['horizontal_lines'] == [77, 91, 105, 119, 133]


def test_can_find_7_bar_lines_in_stave(im, staves):
    assert len(staves(im)[0]['bar_lines']) == 7


def test_can_get_horizontal_spacing_between_lines(im, staves):
    assert staves(im)[0]['spacing'] == 14


def test_can_find_bar_lines_in_stave(im, staves):
    assert staves(im)[0]['bar_lines'] == [402, 515, 628, 741, 854, 967, 1080]


def test_can_find_the_treble_clef(im, staves):
    assert staves(im)[0]['clef'] == ((53, 206), (160, 246))


def test_can_find_the_c_time_signature(im, staves):
    assert staves(im)[0]['time'] == ((76, 265), (135, 291))


def test_can_find_the_b_in_the_second_bar(im, staves):
    notes = find_notes(im, staves(im)[0]).split(" ")
    assert notes[1] == 'b1'


def test_can_find_the_c_in_the_third_bar(im, staves):
    notes = find_notes(im, staves(im)[0]).split(" ")
    assert notes[2] == 'c1'


def test_can_find_the_g_in_the_seventh_bar(im, staves):
    notes = find_notes(im, staves(im)[0]).split(" ")
    assert notes[6] == 'g1'


def test_can_find_the_a_in_the_first_bar(im, staves):
    notes = find_notes(im, staves(im)[0]).split(" ")
    assert notes[0] == 'a1'


def test_can_find_all_notes_in_first_stave(im, staves):
    found_staves = staves(im)
    found_staves[0]['notes'] = find_notes(im, found_staves[0])
    assert found_staves[0]['notes'] == 'a1 b1 c1 d1 e1 f1 g1'


def test_can_write_to_file():
    notes = 'a1 b1 c1 d1 e1 f1 g1'
    write(notes)
    assert pathlib.Path("output.ly").read_text() == pathlib.Path("test.ly").read_text()


def test_get_interval_returns_2_with_distance_of_spacing_over_two():
    assert get_interval(0, 1) == 2


def test_get_interval_returns_3_with_c_and_e():
    assert get_interval(0, 2) == 3


def test_get_interval_returns_3_with_d_and_f():
    assert get_interval(1, 3) == 3


def test_get_interval_returns_4_with_d_and_g():
    assert get_interval(1, 4) == 4


def test_get_interval_returns_minus_4_with_g_and_d():
    assert get_interval(4, 1) == -4


def test_get_interval_returns_8_with_d_and_dup_an_octave():
    assert get_interval(0, 7) == 8


def test_get_interval_returns_9_with_c_and_dup_an_octave():
    assert get_interval(-1, 7) == 9


def test_works_with_descending_notes(descending_notes_im, staves):
    found_staves = staves(descending_notes_im)
    notes = find_notes(descending_notes_im, found_staves[0])
    write(notes)
    assert pathlib.Path("output.ly").read_text() == pathlib.Path("descending.ly").read_text()


def test_works_with_two_octave_jump(octave_jump_im, staves):
    found_staves = staves(octave_jump_im)
    notes = find_notes(octave_jump_im, found_staves[0])
    write(notes)
    assert pathlib.Path("output.ly").read_text() == pathlib.Path("octave_jump.ly").read_text()


def test_can_handle_minims(minim_im, staves):
    found_staves = staves(minim_im)
    notes = find_notes(minim_im, found_staves[0])
    assert notes == "c2 d2"
